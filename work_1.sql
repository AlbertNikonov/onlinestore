-- Создание базы данных
CREATE DATABASE online_store;

-- Создание таблицы "Categories"
CREATE TABLE Categories (
  id SERIAL PRIMARY KEY, 
  name VARCHAR(100) NOT NULL
);

-- Создание таблицы "Products"
CREATE TABLE Products (
  id SERIAL PRIMARY KEY, 
  name VARCHAR(100) NOT NULL, 
  price DECIMAL(10, 2) NOT NULL, 
  description TEXT, 
  category_id INT REFERENCES Categories(id)
);

-- Создание таблицы "Users" (Пользователи)
CREATE TABLE Users (
  id SERIAL PRIMARY KEY, 
  username VARCHAR(100) NOT NULL, 
  email VARCHAR(100) NOT NULL, 
  password VARCHAR(100) NOT NULL, 
  address TEXT
);
-- Создание таблицы "Orders" (Заказы)
CREATE TABLE Orders (
  id SERIAL PRIMARY KEY, 
  user_id INT REFERENCES Users(id), 
  order_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
  status VARCHAR(50) NOT NULL, 
  total_amount DECIMAL(10, 2) NOT NULL
);
-- Создание таблицы "Order_Details" (Детали заказа)
CREATE TABLE Order_Details (
  id SERIAL PRIMARY KEY, 
  order_id INT REFERENCES Orders(id), 
  product_id INT REFERENCES Products(id), 
  quantity INT NOT NULL, 
  price DECIMAL(10, 2) NOT NULL
);

-- Заполнение таблицы "Categories" данными
INSERT INTO Categories (name) 
VALUES 
  ('Одежда'), 
  ('Обувь'), 
  ('Электроника'), 
  ('Косметика');
-- Заполнение таблицы "Products" данными
INSERT INTO Products (
  name, price, description, category_id
) 
VALUES 
  (
    'Пальто', 2500.00, 'Теплое пальто для холодной погоды', 
    1
  ), 
  (
    'Футболка', 500.00, 'Хлопковая футболка с принтом', 
    1
  ), 
  (
    'Кеды', 1200.00, 'Комфортные кеды для повседневной носки', 
    2
  ), 
  (
    'Смартфон', 15000.00, 'Современный смартфон с высокими техническими характеристиками', 
    3
  ), 
  (
    'Ноутбук', 35000.00, 'Легкий и мощный ноутбук для работы и развлечений', 
    3
  ), 
  (
    'Тушь для ресниц', 
    300.00, 'Тушь, делающая ресницы длинными и объемными', 
    4
  );

-- Заполнение таблицы "Users" данными
INSERT INTO Users (
  username, email, PASSWORD, address
) 
VALUES 
  (
    'alex', 'alex@mail.com', 'BestPass123', 
    'г.Москва'
  ), 
  (
    'max', 'max@mail.com', 'StrongPass123', 
    'г.Санкт-Петербург'
  ), 
  (
    'anya', 'anya@mail.com', 'MyGoodPass123', 
    'г.Казань'
  );
-- Заполнение таблицы "Orders" данными
INSERT INTO Orders (user_id, status, total_amount) 
VALUES 
  (
    1, 'В обработке', 1500.00
  ), 
  (2, 'Отправлен', 2500.00), 
  (3, 'Доставлен', 1800.00);
-- Заполнение таблицы "Order_Details" данными
INSERT INTO Order_Details (
  order_id, product_id, quantity, price
) 
VALUES 
  (1, 1, 2, 1000.00), 
  (1, 3, 1, 500.00), 
  (2, 2, 1, 1500.00), 
  (3, 1, 1, 800.00), 
  (3, 2, 1, 1000.00);
  
  
 
-- Извлечение данных из таблицы "Products"
SELECT 
  Products.id, 
  Products.name, 
  Products.price, 
  Products.description, 
  Categories.name AS category 
FROM 
  Products 
  JOIN Categories ON Products.category_id = Categories.id;
 
-- Извлечение данных из таблицы "Users"
SELECT 
  * 
FROM 
  Users;
-- Извлечение данных из таблицы "Orders"
SELECT 
  * 
FROM 
  Orders;
-- Извлечение данных из таблицы "Order_Details"
SELECT
	*
FROM
	Order_Details;
